##  Tutorials on Deep Learning for NLP


This is a series of tutorials to introduce basic neural network concepts and methods used in NLP.
We use TensorFlow as an illustration for those concepts. These are not TensorFlow tutorials. 
We only use TensorFlow low level API as it does not hide too much details of neural network concepts. 

* Tutorial 0 - Document Classification: This tutorial introduces neural network concepts and the usage of word embedding in NLP tasks.
* Tutorial 1 - N-gram language model: This tutorial
* Tutorial 2 - RNN language model
* Tutorial 3 - Sequence-to-sequence model

### Prerequisites
Jupyter notebook and Tensorflow

#### Jupyter notebook installation
   
    
    pip install jupyter
    
    For more information, look into [Jupyter website](https://jupyter.readthedocs.io/en/latest/install.html)
   

#### TensorFlow Installation
    pip install tensorflow
For more information, look into [Tensorflow website](https://www.tensorflow.org/install/)

#### Dependent python package
    pip install nltk sklearn numpy
    
To start a tutorial. For all tutorials, we assume that seq2seq-tut is our working directory.

    jupyter notebook <tutorial-file>


### Dataset Structure
   * Tutorial 0: 
        * [Sentiment Labelled Sentences Dataset](https://archive.ics.uci.edu/ml/datasets/Sentiment+Labelled+Sentences): ./data/tut0
        * [GloVe embedding](https://nlp.stanford.edu/projects/glove/): ./data/embedding
   * Tutorial 1 & 2:
        * [PTB dataset](https://github.com/tomsercu/lstm/tree/master/data): ./data/ptb
   * Tutorial 3:
        * English-German parallel corpus: ./data/mt
        
### Other resources:
    * NLP book: https://www-morganclaypool-com.ezproxy.lib.monash.edu.au/doi/pdf/10.2200/S00762ED1V01Y201703HLT037
    * RNN resource: https://docs.google.com/document/d/1oNzTexwii1KhswrOveSqZVaOIgWhJh2f4vUiH8DJmlc/edit
    * Original paper on RNN LM: http://www.jmlr.org/papers/volume3/bengio03a/bengio03a.pdf
    https://arxiv.org/abs/1706.03762
    


   
   
